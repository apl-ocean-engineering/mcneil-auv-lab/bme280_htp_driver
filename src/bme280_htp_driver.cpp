/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "bme280_htp_driver/bme280_htp_driver.h"

#include <fcntl.h>  // O_RDWR
#include <sys/ioctl.h>
#include <unistd.h>

#include "bme280_htp_driver/linux_userspace.h"
#include "bosch_sensortec_msgs/Bme280Htp.h"
#include "ros/ros.h"
#include "sensor_msgs/FluidPressure.h"
#include "sensor_msgs/RelativeHumidity.h"
#include "sensor_msgs/Temperature.h"

Bme280HtpDriver::Bme280HtpDriver() {
  nh_ = ros::NodeHandle("~");

  setupParams();

  htp_pub_ = nh_.advertise<bosch_sensortec_msgs::Bme280Htp>("htp", 10);
  temperature_pub_ = nh_.advertise<sensor_msgs::Temperature>("temperature", 10);
  pressure_pub_ = nh_.advertise<sensor_msgs::FluidPressure>("pressure", 10);
  humidity_pub_ = nh_.advertise<sensor_msgs::RelativeHumidity>("humidity", 10);

  if (use_spi_) {
    setupSPI();
  } else {
    setupI2C();
  }
  configureBme280();
}

void Bme280HtpDriver::setupParams() {
  nh_.getParam("rate_hz", rate_hz_);
  nh_.getParam("use_spi", use_spi_);
  nh_.getParam("dev_addr", dev_addr_);
  if (!use_spi_) {
    nh_.getParam("i2c_primary", i2c_primary_);
  }
  ROS_INFO_STREAM("Publishing HTP data at " << rate_hz_ << " Hz, from device "
                                            << dev_addr_ << ".");
}

void Bme280HtpDriver::setupSPI() {
  id_.fd = open(dev_addr_.c_str(), O_RDWR);
  if (id_.fd < 0) {
    ROS_FATAL_STREAM("Could not open SPI bus at " << dev_addr_ << ". Exiting.");
    ros::shutdown();
  } else {
    ROS_INFO_STREAM("Opened SPI port at " << dev_addr_);
  }
}

void Bme280HtpDriver::setupI2C() {
  id_.fd = open(dev_addr_.c_str(), O_RDWR);
  if (id_.fd < 0) {
    ROS_FATAL_STREAM("Could not open i2c bus at " << dev_addr_ << ". Exiting.");
    ros::shutdown();
  } else {
    ROS_INFO_STREAM("Opened I2C port at " << dev_addr_);
  }

  if (i2c_primary_) {
    id_.dev_addr = BME280_I2C_ADDR_PRIM;
  } else {
    id_.dev_addr = BME280_I2C_ADDR_SEC;
  }
  if (ioctl(id_.fd, I2C_SLAVE, id_.dev_addr) < 0) {
    ROS_FATAL_STREAM("Failed to set i2c device address");
    ros::shutdown();
  }
}

void Bme280HtpDriver::configureBme280() {
  if (use_spi_) {
    device_.intf = BME280_SPI_INTF;
    device_.read = user_spi_read;
    device_.write = user_spi_write;
  } else {
    device_.intf = BME280_I2C_INTF;
    // The device needs pointers back to functions used for
    // reading/writing
    device_.read = user_i2c_read;
    device_.write = user_i2c_write;
  }
  device_.delay_us = userDelayMicroseconds;

  // The bosch library will call provided read/write functions with this
  // pointer as an argument.
  device_.intf_ptr = &id_;

  // Use Bosch-provided function to initialize the BME280
  int8_t result;
  result = bme280_init(&device_);
  if (result != BME280_OK) {
    ROS_FATAL_STREAM(
        "Failed to initialize the device. Code: " << unsigned(result));
    ros::shutdown();
  }

  // Get the current sensor settings, then use that struct to make needed
  // changes. This ensures that all fields are set to something
  // reasonable.
  struct bme280_settings settings = {0};
  result = bme280_get_sensor_settings(&settings, &device_);
  if (result != BME280_OK) {
    ROS_FATAL_STREAM("Failed to get sensor settings. code: " << result);
    ros::shutdown();
  }

  // Using the recommended parameters from linux_userspace.c
  // I don't think this matters terribly for our application
  settings.filter = BME280_FILTER_COEFF_16;
  settings.osr_h = BME280_OVERSAMPLING_1X;
  settings.osr_p = BME280_OVERSAMPLING_16X;
  settings.osr_t = BME280_OVERSAMPLING_2X;

  result =
      bme280_set_sensor_settings(BME280_SEL_ALL_SETTINGS, &settings, &device_);
  if (result != BME280_OK) {
    ROS_FATAL_STREAM("Failed to set sensor settings. Code: " << result);
    ros::shutdown();
  }

  // Calculate the minimum delay required between consecutive measurement
  // based upon the sensor enabled and the oversampling configuration.
  bme280_cal_meas_delay(&required_delay_us_, &settings);
  ROS_INFO_STREAM("Current configuration requires delay of "
                  << required_delay_us_ << " us.");
}

void Bme280HtpDriver::run() {
  ros::Rate htp_rate = ros::Rate(rate_hz_);
  while (ros::ok()) {
    readHtp();
    htp_rate.sleep();
  }
}

void Bme280HtpDriver::readHtp() {
  // Structure to get the pressure, temperature and humidity values
  struct bme280_data comp_data;

  // Request data
  uint8_t result = bme280_set_sensor_mode(BME280_POWERMODE_FORCED, &device_);
  if (result != BME280_OK) {
    ROS_ERROR_STREAM("Failed to set sensor mode. Code: " << result);
    // TODO(lindzey): Is there any recovery that can be done at this
    // point?
    //   Given that we've already successfully configured the sensor, this
    //   *should* work.
    return;
  }

  // Wait for the measurement to complete and print data
  ros::Duration(1e-6 * required_delay_us_).sleep();

  result = bme280_get_sensor_data(BME280_ALL, &comp_data, &device_);
  if (result != BME280_OK) {
    ROS_ERROR_STREAM("Failed to get sensor data. Code: " << result);
    return;
  }

  ros::Time msg_stamp = ros::Time::now();

  bosch_sensortec_msgs::Bme280Htp htp_msg;
  htp_msg.header.stamp = msg_stamp;
  htp_msg.temperature = comp_data.temperature;
  htp_msg.pressure = comp_data.pressure;
  htp_msg.humidity = comp_data.humidity;
  htp_pub_.publish(htp_msg);

  sensor_msgs::Temperature temperature_msg;
  temperature_msg.header.stamp = msg_stamp;
  temperature_msg.temperature = comp_data.temperature;
  temperature_msg.variance = 0;  // variance unknown
  temperature_pub_.publish(temperature_msg);

  sensor_msgs::FluidPressure pressure_msg;
  pressure_msg.header.stamp = msg_stamp;
  pressure_msg.fluid_pressure = comp_data.pressure;  // Pascals
  pressure_msg.variance = 0;                         // variance unknown
  pressure_pub_.publish(pressure_msg);

  sensor_msgs::RelativeHumidity humidity_msg;
  humidity_msg.header.stamp = msg_stamp;
  // BME280 reports in percent, while sensor_msgs expects [0.0, 1.0].
  humidity_msg.relative_humidity = comp_data.humidity / 100;
  humidity_msg.variance = 0;  // variance unknown
  humidity_pub_.publish(humidity_msg);
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "bme280_htp_driver");
  Bme280HtpDriver htp;
  htp.run();
  return 0;
}
