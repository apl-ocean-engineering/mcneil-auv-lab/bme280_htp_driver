## bme280_htp_driver

ROS driver for the Bosch BME280 environmental sensor.

This code builds upon the driver that Bosch provides:
https://github.com/boschsensortec/BME280_driver

Rather than messsing with git submodules, we have copied the
relevant files into this repo, with their copyright statements
and BSD-3-clause license statements intact.

**Parameters**:
* rate_hz: Rate to sample the pin at, in Hz.
* dev_addr: I2C bus address, e.g. /dev/i2c-0
* i2c_primary: whether the BME280 is configured to use the
               primary or secondary I2C address

**Publications**:
In addition to a single message containing all fields received from the device,
this driver publishes humidity, temperature and pressure using ROS's standard
sensor_msgs.

* ~/htp (`bosch_sensortec_msgs/Bme280Htp.msg`): combined Humidity, Temperature and Pressure message from a single reading
* ~/temperature (`sensor_msgs/Temperature.msg`): temperature in deg C
* ~/pressure (`sensor_msgs/FluidPressure.msg`): pressure in Pascals
* ~/humidity (`sensor_msgs/RelativeHumidity.msg`): relative humidity (0.0 - 1.0)
