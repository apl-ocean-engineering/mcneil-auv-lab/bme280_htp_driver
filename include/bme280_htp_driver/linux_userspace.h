/**\
 * Copyright (c) 2020 Bosch Sensortec GmbH. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 **/

// NOTE: This was extracted from linux_userspace.c so I could use some of those
//    functions in the driver.

#ifndef INCLUDE_BME280_HTP_DRIVER_LINUX_USERSPACE_H_
#define INCLUDE_BME280_HTP_DRIVER_LINUX_USERSPACE_H_

#include <bme280_htp_driver/bme280.h>
#include <bme280_htp_driver/bme280_defs.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <linux/spi/spidev.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>

/* Structure that contains identifier details used in example */
struct identifier {
  /* Variable to hold device address */
  uint8_t dev_addr;

  /* Variable that contains file descriptor */
  int8_t fd;
};

/*!
 *  @brief Function that creates a mandatory delay required in some of the APIs.
 *
 * @param[in] period              : Delay in microseconds.
 * @param[in, out] intf_ptr       : Void pointer that can enable the linking of
 * descriptors for interface related call backs
 *  @return void.
 *
 */
void user_delay_us(uint32_t period, void *intf_ptr) { usleep(period); }

/*!
 *  @brief Function for reading the sensor's registers through I2C bus.
 *
 *  @param[in] reg_addr       : Register address.
 *  @param[out] data          : Pointer to the data buffer to store the read
 * data.
 *  @param[in] len            : No of bytes to read.
 *  @param[in, out] intf_ptr  : Void pointer that can enable the linking of
 * descriptors for interface related call backs.
 *
 *  @return Status of execution
 *
 *  @retval 0 -> Success
 *  @retval > 0 -> Failure Info
 *
 */
int8_t user_i2c_read(uint8_t reg_addr, uint8_t *data, uint32_t len,
                     void *intf_ptr) {
  struct identifier id;

  id = *((struct identifier *)intf_ptr);
  write(id.fd, &reg_addr, 1);
  if (read(id.fd, data, len) != len) {
    return -1;
  }

  return 0;
}
int8_t user_spi_read(uint8_t reg_addr, uint8_t *data, uint32_t len,
                     void *intf_ptr) {
  // fprintf(stderr, "user_spi_read\n");
  struct identifier id;
  id = *((struct identifier *)intf_ptr);

  // The first byte of the transfer is the register address;
  // the following bytesa re clocked by the microcontroller so the peripheral
  // can respond. I tried doing it as two different transfers, and that didn't
  // work.
  struct spi_ioc_transfer tr;
  memset(&tr, 0, sizeof(tr));

  // first byte will be reg_addr, rest initialized to 0
  uint8_t *tx = static_cast<uint8_t *>(malloc(len + 1));
  memset(tx, 0, sizeof(tx));
  tx[0] = reg_addr;
  uint8_t *rx = static_cast<uint8_t *>(malloc(len + 1));

  // cpplint doesn't like the C-style types, and a C++-style
  // static_cast<unsigned long long>(...) doesn't compile.
  // Cast to the pointer type used by the spi_ioc_transfer struct.
  // (It uses __u64)
  tr.tx_buf = (unsigned long long)tx;  // NOLINT
  tr.rx_buf = (unsigned long long)rx;  // NOLINT
  tr.len = len + 1;
  tr.delay_usecs = 0;
  tr.speed_hz = 500000;
  tr.bits_per_word = 8;
  tr.cs_change = 0;

  int ret = ioctl(id.fd, SPI_IOC_MESSAGE(1), &tr);

  /**
  for (int ii = 0; ii < len+1; ii ++) {
    fprintf(stderr, "tx[%d] = 0x%02X\n", ii, tx[ii]);
  }
  for (int ii = 0; ii < len+1; ii ++) {
    fprintf(stderr, "rx[%d] = 0x%02X\n", ii, rx[ii]);
  }
  */

  memcpy(data, rx + 1, len);

  free(tx);
  free(rx);

  if (ret < 0) {
    return BME280_E_COMM_FAIL;
  } else {
    return BME280_OK;
  }
}

/*!
 *  @brief Function for writing the sensor's registers through I2C bus.
 *
 *  @param[in] reg_addr       : Register address.
 *  @param[in] data           : Pointer to the data buffer whose value is to be
 * written.
 *  @param[in] len            : No of bytes to write.
 *  @param[in, out] intf_ptr  : Void pointer that can enable the linking of
 * descriptors for interface related call backs
 *
 *  @return Status of execution
 *
 *  @retval BME280_OK -> Success
 *  @retval BME280_E_COMM_FAIL -> Communication failure.
 *
 */
int8_t user_i2c_write(uint8_t reg_addr, const uint8_t *data, uint32_t len,
                      void *intf_ptr) {
  uint8_t *buf;
  struct identifier id;

  id = *((struct identifier *)intf_ptr);

  buf = static_cast<uint8_t *>(malloc(len + 1));
  buf[0] = reg_addr;
  memcpy(buf + 1, data, len);
  if (write(id.fd, buf, len + 1) < (uint16_t)len) {
    return BME280_E_COMM_FAIL;
  }

  free(buf);

  return BME280_OK;
}
int8_t user_spi_write(uint8_t reg_addr, const uint8_t *data, uint32_t len,
                      void *intf_ptr) {
  struct identifier id;
  id = *((struct identifier *)intf_ptr);

  struct spi_ioc_transfer tr;
  memset(&tr, 0, sizeof(tr));
  // first byte will be reg_addr, rest initialized to 0
  uint8_t *tx = static_cast<uint8_t *>(malloc(len + 1));
  tx[0] = reg_addr;
  memcpy(tx + 1, data, len);
  uint8_t *rx = static_cast<uint8_t *>(malloc(len + 1));

  tr.tx_buf = (unsigned long long)tx;  // NOLINT
  tr.rx_buf = (unsigned long long)rx;  // NOLINT
  tr.len = len + 1;
  tr.delay_usecs = 0;
  tr.speed_hz = 500000;
  tr.bits_per_word = 8;
  tr.cs_change = 0;

  int ret = ioctl(id.fd, SPI_IOC_MESSAGE(1), &tr);

  free(tx);
  free(rx);
  if (ret < 0) {
    return BME280_E_COMM_FAIL;
  } else {
    return BME280_OK;
  }
}

#endif  // INCLUDE_BME280_HTP_DRIVER_LINUX_USERSPACE_H_
