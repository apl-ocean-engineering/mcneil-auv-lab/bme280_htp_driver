/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef INCLUDE_BME280_HTP_DRIVER_BME280_HTP_DRIVER_H_
#define INCLUDE_BME280_HTP_DRIVER_BME280_HTP_DRIVER_H_

#include <map>
#include <mutex>
#include <set>
#include <string>
#include <vector>

#include "bme280_htp_driver/bme280.h"
#include "bme280_htp_driver/linux_userspace.h"
#include "ros/ros.h"

// Delay function; the Bosch API requires this.
// * period_us: period in microseconds
// * intf_ptr: Bosch API requires this; enables called functions to
//      have access back into the higher-level driver. (Unused here.)
void userDelayMicroseconds(uint32_t period_us, void *intf_ptr) {
  ros::Duration(1e-6 * period_us).sleep();
}

class Bme280HtpDriver {
 public:
  Bme280HtpDriver();
  // We have a mutex as a member variable, so copy contstructor is forbidden.
  Bme280HtpDriver(const Bme280HtpDriver &) = delete;
  ~Bme280HtpDriver() = default;

  void run();

 private:
  // Setup all ROS parameters
  void setupParams();
  // Set up connection to I2C bus
  void setupI2C();
  // Set up connection to SPI bus
  void setupSPI();
  // Configure the BME280 sensor
  void configureBme280();
  // Read data from device
  void readHtp();

  // Whether to use SPI or I2C
  bool use_spi_;

  // i2c or SPI device to read from. e.g. /dev/i2c-0, /dev/spidev0.0
  std::string dev_addr_;
  // Whether to use the primary or secondary i2c addesss (BME280 only has to)
  bool i2c_primary_;

  // Struct containing device info required by the Bosch-provided functions
  struct bme280_dev device_;
  struct identifier id_;

  // Rate at which to query the BME280
  double rate_hz_;
  // Required sleep time between requesting a measurement and reading it.
  // This will be read from the device after configuring.
  uint32_t required_delay_us_;

  ros::NodeHandle nh_;
  ros::Publisher htp_pub_;
  ros::Publisher temperature_pub_;
  ros::Publisher pressure_pub_;
  ros::Publisher humidity_pub_;
};

#endif  //  INCLUDE_BME280_HTP_DRIVER_BME280_HTP_DRIVER_H_
